import CoingeckoTable from "../components/page-components/CoingeckoTable.vue"
import Tweet from "../components/page-components/Tweet.vue"
import utils from "../utils"
import TelegramIcon from "../assets/images/icons/social-telegram-icon-hover.svg?inline"
import GithubIcon from "../assets/images/icons/social-github-icon.svg?inline"
import GitlabIcon from "../assets/images/icons/social-gitlab-icon.svg?inline"
import TwitterIcon from "../assets/images/icons/social-twitter-icon.svg?inline"

{/* <script async src="https://telegram.org/js/telegram-widget.js?14" data-telegram-discussion="durov/126" data-comments-limit="5" data-colorful="1" data-color="F3BA2F" data-dark="1"></script> */}
export default{
    head() {
      return {
        
      }
    },
    components: { 
        CoingeckoTable,
        Tweet,
        TelegramIcon,
        GithubIcon,
        GitlabIcon,
        TwitterIcon
     },
    mounted(){
        console.log("homepage mounted")
        console.log(
            this.activeNodeCount,
            this.standbyNodeCount,
            this.totalCapitalUsd,
            this.totalBondedUsd,
            this.totalPooledUsd,
            this.volume24h,
            this.tx24h,
            this.users24h)

        this.whichCard = Math.floor((Math.random() * 10) + 1);
    },
    async fetch() {
        
        // filling up Tools Table
        var toolsTableContent = this.$t('homePage.toolsTableContent')
        for(var key of Object.keys(toolsTableContent)){
          var record = toolsTableContent[key]
          record['link'] = this.$links(`tools_table.${key}.tool_link`)
          record['repo_url'] = this.$links(`tools_table.${key}.repo_url`)
          record['telegram_url'] = this.$links(`tools_table.${key}.telegram_url`)
          record['github_url'] = this.$links(`tools_table.${key}.github_url`)
          record['gitlab_url'] = this.$links(`tools_table.${key}.gitlab_url`)
          record['twitter_url'] = this.$links(`tools_table.${key}.twitter_url`)
          console.log(record)
          this.toolsTable.push(record)
        }
        this.toolsTable.sort(function(a, b){
          if(a.category < b.category) { return -1; }
          if(a.category > b.category) { return 1; }
          return 0;
        })
        console.log(this.toolsTable)
    },
    methods:{
      titleButSirMouseoverEvent(event){
        this.titleButSirMouseover = true;
      },
      titleButSirMouseoutEvent(event){
        this.titleButSirMouseover = false;
      },
      titleButSirClicked(event){
        this.$store.commit('showYoutubeOverlay', true)
      }
    },
    async asyncData({$services}){
      var minimumBond = await $services.getMinimumBond()
      var netValues = await $services.getThorNetValues()
      var runePrice = await $services.getRunePrice()
      if(netValues){
        var nodesAPY = parseInt(netValues.bondingAPY * 100)
        var lpAPY = parseInt(netValues.liquidityAPY * 100)
        var activeNodeCount = netValues.activeNodeCount
        var standbyNodeCount = netValues.standbyNodeCount
      }
      if(netValues && runePrice){
        var totalStaked = netValues.totalStaked / 100000000
        totalStaked = parseInt(totalStaked * runePrice)
        totalStaked = utils.numberWithCommas(totalStaked)
        var totalPooledUsd = totalStaked
        
        var totalCapital = parseInt(netValues.totalCapital / 100000000)
        var totalCapitalUSD = parseInt(totalCapital * runePrice)
        var totalCapitalTxt = utils.numberWithCommas(totalCapitalUSD)
        var totalCapitalUsd = totalCapitalTxt
        
        var totalBonded = parseInt(netValues.totalActiveBond / 100000000)
        totalBonded = parseInt(totalBonded * runePrice)
        var totalBondedUsd = utils.numberWithCommas(totalBonded)
        
        var volume = parseInt(netValues.volume24h / 100000000)
        volume = parseInt(volume * runePrice)
        var volume24h = utils.nFormatter(volume)
        var tx24h = utils.nFormatter(netValues.totalTx24h)
        var users24h = utils.nFormatter(netValues.totalUsers)
      }
      if(minimumBond && netValues && runePrice){

        var nodeIncomeYearly = netValues.bondingAPY * minimumBond
        var nodeIncomeYearlyUSD = (nodeIncomeYearly * runePrice)
        var nodeIncomeDaily = (nodeIncomeYearly / 365)
        var nodeIncomeDailyUSD = (nodeIncomeDaily * runePrice)
        
        nodeIncomeYearly = utils.numberWithCommas(parseInt(nodeIncomeYearly))
        nodeIncomeYearlyUSD = utils.numberWithCommas(parseInt(nodeIncomeYearlyUSD))
        nodeIncomeDaily = utils.numberWithCommas(parseInt(nodeIncomeDaily))
        nodeIncomeDailyUSD = utils.numberWithCommas(parseInt(nodeIncomeDailyUSD))
        
        var nodeTweetText = `My $RUNE node is currently printing me ${nodeIncomeDaily} $RUNE per day, roughly $${nodeIncomeDailyUSD}/day

        That’s $${nodeIncomeYearlyUSD}/year
        Haters mad`
        var nodeTweetDate = new Date().toLocaleString('en', {day: 'numeric', month: 'short'})
      }
      else{
        var nodeTweetText = `My $RUNE node is currently printing me 860 $RUNE per day, roughly $1,550/day

        That’s $565,000/year
        Haters mad`
        var nodeTweetDate = "Jan 13"
      }

      var coingecko = await $services.getCoingeckoData()
      if(coingecko){
        var circulatingSupply = parseInt(coingecko.netInfo.circulatingSupply)
        var runeLocked = (totalCapital / circulatingSupply) * 100
        runeLocked = parseInt(runeLocked)
      }
      return {
        nodeTweetText,
        nodeTweetDate,
        nodesAPY,
        lpAPY,
        activeNodeCount,
        standbyNodeCount,
        totalPooledUsd,
        totalCapitalUsd,
        totalBondedUsd,
        volume24h,
        tx24h,
        users24h,
        runeLocked
      }        
    },
    data(){
        return {
            titleButSirMouseover: false,
            nodesAPY: 23,
            lpAPY: 123,
            activeNodeCount: 0,
            standbyNodeCount: 0,
            totalCapitalUsd: 0,
            totalBondedUsd: 0,
            totalPooledUsd: 0,
            volume24h: 0,
            tx24h: 0,
            users24h: 0,
            runeLocked: 0,
            whichCard: 0,
            toolsTable:[],
            nodeTweetText: ""
         }
    },
    head() {
        return {

            title: 'THORChain',
            meta: [
                    {property: "og:locale", content: "en_US"},
                    {property: "og:type", content: "website"},
                    {property: "og:title", content: "THORChain"},
                    {property: "og:description", content: "THORChain is a decentralized liquidity network. Deposit native assets into Liquidity Pools to earn yield. The network is 100% autonomous and decentralized."},
                    {property: "og:url", content: process.env.baseUrl},
                    {property: "og:site_name", content: "THORChain"},
                    {property: "og:image", content: process.env.baseUrl + "/homepage-metacard.png"},
                    {property: "og:image:width", content: "876"},
                    {property: "og:image:heigh", content: "438"},
                    {name: "twitter:creator", content: "@thorchain_org"},
                    {name: "twitter:site", content: "@thorchain_org"},
                    {name: "twitter:title", content: "THORChain"},
                    {name: "twitter:description", content: "THORChain is a decentralized liquidity network. Deposit native assets into Liquidity Pools to earn yield. The network is 100% autonomous and decentralized."},
                    {name: "twitter:card", content: "summary_large_image"},
                    {name: "twitter:image", content: process.env.baseUrl + "/homepage-metacard.png"},
            ],
        }
    },
}