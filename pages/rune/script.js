import utils from '../../utils';
import CoingeckoTable from "../../components/page-components/CoingeckoTable.vue";
export default {
  components:{
    CoingeckoTable,
  },
  data() {
    return {
      circulatingSupply: 10,
      runeLocked: 0,
      circulatingSupplyTxt: 0,
      avatars: [
        {
          img: "https://pbs.twimg.com/profile_images/1352210026169917440/63UOlW-r_400x400.png",
          name: 'ThorChain',
          tag: 'Introducing Community Pros!',
          socialLinks: {
              github: '',
              gitlab: '',
              telegram: '',
              twitter: ''
          },
          active: false,
          thorchain: true
        },
        {
            img: require('~/assets/images/external-avatars/bitcoin-sage.jpg'),
            name: 'Bitcoin_Sage',
            tag: '@Bitcoin_Sage',
            socialLinks: {
                github: '',
                gitlab: '',
                telegram: this.$links("telegram_bitcoin_sage"),
                twitter: this.$links("twitter_bitcoin_sage")
            },
            active: false
        },
        {
            img: "https://pbs.twimg.com/profile_images/1223357474415181824/WtbMne0F_400x400.jpg",
            name: 'Delphi_Digital',
            tag: '@Delphi_Digital',
            socialLinks: {
                github: '',
                gitlab: '',
                telegram: this.$links("telegram_delphi_digital"),
                twitter: this.$links("twitter_delphi_digital")
            },
            active: false
        },
        {
            img: require('~/assets/images/external-avatars/larry.jpg'),
            name: '_0x_larry',
            tag: '@_0x_larry',
            socialLinks: {
                github: this.$links("github_larrypcdotcom"),
                gitlab: '',
                telegram: this.$links("telegram_larrypc"),
                twitter: this.$links("twitter__0x_larry")
            },
            active: false
        },
        {
            img: "https://pbs.twimg.com/profile_images/1351126832582684673/y__ZUtTJ_400x400.jpg",
            name: 'Jonny_Qi',
            tag: '@Jonny_Qi',
            socialLinks: {
                github: '',
                gitlab: '',
                telegram: '',
                twitter: this.$links("twitter_jonny_qi")
            },
            active: false
        },
        {
          img: "https://pbs.twimg.com/profile_images/1298659499323121668/u8kKrMpA_400x400.jpg",
          name: 'CanGure91596181',
          tag: '@CanGure91596181',
          socialLinks: {
              gitlab: '',
              github: '',
              telegram: '',
              twitter: this.$links("twitter_cangure91596181")
          },
          active: false
        },
        {
          img: require("~/assets/images/avatars/sample-avatar.svg"),
          name: 'tannedoaksprout',
          tag: '@tannedoaksprout',
          socialLinks: {
              gitlab: '',
              github: '',
              telegram: '',
              twitter: this.$links("twitter_tannedoaksprout")
          },
          active: false
        },
        {
          img: "https://pbs.twimg.com/profile_images/1259804717678424070/L-ihu83N_400x400.jpg",
          name: 'Jatinkkalra',
          tag: '@Jatinkkalra',
          socialLinks: {
              gitlab: '',
              github: '',
              telegram: this.$links("telegram_jatinkkalra"),
              twitter: this.$links("twitter_jatinkkalra")
          },
          active: false
        },
      ]
    }
  },
  head(){
    return {

    title: "(ᚱ) RUNE Token - THORChain",
    meta: [
{ property: "og:locale", content: "en_US"},
{ property: "og:type", content:"website"},
{ property: "og:title", content:"(ᚱ) RUNE Token - THORChain"},
{ property: "og:description",content:"(ᚱ) RUNE is the asset which powers the THORChain ecosystem and provides the economic incentives required to secure the network."},
{ property: "og:url", content: process.env.baseUrl + "/rune"},
{ property: "og:site_name", content:"THORChain"},
{ property: "og:image", content: process.env.baseUrl + "/rune-meta.png"},
{ property: "og:image:width", content:"876"},
{ property: "og:image:height", content:"438"},
{ name: "twitter:creator", content:"@thorchain_org"},
{ name: "twitter:site", content:"@thorchain_org"},
{ name: "twitter:title", content:"(ᚱ) RUNE Token - THORChain"},
{ name: "twitter:description", content:"(ᚱ) RUNE is the asset which powers the THORChain ecosystem and provides the economic incentives required to secure the network."},
{ name: "twitter:card", content:"summary_large_image"},
{ name: "twitter:image", content: process.env.baseUrl + "/rune-meta.png"}
    ]
  }

  },
  
  async fetch() {
    var res = await this.$services.getCoingeckoData()
    console.log(res)
    if(!res || !res.netInfo) return
    this.circulatingSupply = parseInt(res.netInfo.circulatingSupply)
    this.circulatingSupplyTxt = utils.numberWithCommas(this.circulatingSupply)

    this.runeLocked = (this.totalCapital / this.circulatingSupply) * 100
    this.runeLocked = parseInt(this.runeLocked)
    console.log("RuneLocked", this.runeLocked)
  },
  async asyncData({ app, $services }) {
    var netValues = await $services.getThorNetValues()
    var runePrice = await $services.getRunePrice()

    if (runePrice && netValues) {

      var totalStaked = netValues.totalStaked / 100000000
      totalStaked = parseInt(totalStaked * runePrice)
      totalStaked = utils.numberWithCommas(totalStaked)

      var totalCapital = parseInt(netValues.totalCapital / 100000000)
      var totalCapitalTxt = utils.numberWithCommas(totalCapital)

      var nodeAPY = parseInt(netValues.bondingAPY * 100)
      var liquidAPY = parseInt(netValues.liquidityAPY * 100)

      var totalEarned = netValues.totalEarned / 100000000
      var totalEarnedUsd = parseInt(totalEarned * runePrice)
      var totalEarnedTxt = utils.numberWithCommas(totalEarnedUsd)


      var retVal = { nonRuneTVL: totalStaked, totalCapital, totalCapitalTxt, nodeAPY, liquidAPY, totalEarnedTxt }
      // console.log(retVal)
      return retVal
    }
    else{
      return {
        nonRuneTVL: 0,
        totalCapital: 0,
        totalCapitalTxt: "unknown",
        nodeAPY: 0,
        liquidAPY:0,
        totalEarnedTxt: "unknown"
      }
    }

  },
  mounted() {
    console.log("Rune page mounted. is production: "+ process.env.isProduction);

    window.addEventListener('mousedown', (e) => {
        // console.log(e.target)
        console.log(e.target.compareDocumentPosition(document.querySelector(".community-pros")))
        if (e.target.compareDocumentPosition(document.querySelector(".community-pros")) !== 10){
            this.clickOutside();
        }
    });
  },
  methods: {
    handleScroll() {
      this.$store.commit({
        type: "scrollChange",
        amount: window.scrollY,
      });
    },
    showThisAvatar(i) {
        this.avatars.forEach((ava, index) => {
            if (index == i) {
                if (ava.active) {
                    ava.active = false
                    return
                }
                ava.active = true
            }
            else
                ava.active = false
        })
    },
    clickOutside() {
        this.avatars.forEach(ava => {
            ava.active = false;
        })
    }
  },
  beforeMount() {
    this.handleScroll();
    window.addEventListener("scroll", this.handleScroll);
    // console.log("beforeMount");
  },
  beforeDestroy() {
    window.removeEventListener("scroll", this.handleScroll);
    // console.log("beforeDestroy");
  },
};