import utils from "~/utils";
export default {
    data() {
        return {
            minimumBond: null,
            bondInUsd: null,
            asgardActiveVaultLink: "dyanmic",
            avatars: [
                {
                    img: "https://pbs.twimg.com/profile_images/1352210026169917440/63UOlW-r_400x400.png",
                    name: 'ThorChain',
                    tag: 'Introducing Community Pros!',
                    socialLinks: {
                        github: '',
                        gitlab: '',
                        telegram: '',
                        twitter: ''
                    },
                    active: false,
                    thorchain: true
                },
                {
                    img: "https://pbs.twimg.com/profile_images/1223357474415181824/WtbMne0F_400x400.jpg",
                    name: 'Delphi_Digital',
                    tag: '@Delphi_Digital',
                    socialLinks: {
                        github: '',
                        gitlab: '',
                        telegram: this.$links("telegram_delphi_digital"),
                        twitter: this.$links("twitter_delphi_digital")
                    },
                    active: false
                },
                {
                    img: "https://pbs.twimg.com/profile_images/928051647292497920/zX2X7OaN_400x400.jpg",
                    name: 'mperklin',
                    tag: '@mperklin',
                    socialLinks: {
                        github: '',
                        gitlab: '',
                        telegram: this.$links("telegram_mperklin"),
                        twitter: this.$links("twitter_mperklin")
                    },
                    active: false
                },
                {
                    img: require("~/assets/images/avatars/sample-avatar.svg"),
                    name: 'tannedoaksprout',
                    tag: '@tannedoaksprout',
                    socialLinks: {
                        gitlab: '',
                        github: '',
                        telegram: '',
                        twitter: this.$links("twitter_tannedoaksprout")
                    },
                    active: false
                },
                {
                    img: "https://pbs.twimg.com/profile_images/1298659499323121668/u8kKrMpA_400x400.jpg",
                    name: 'CanGure91596181',
                    tag: '@CanGure91596181',
                    socialLinks: {
                        gitlab: '',
                        github: '',
                        telegram: '',
                        twitter: this.$links("twitter_cangure91596181")
                    },
                    active: false
                },
                {
                    img: "https://pbs.twimg.com/profile_images/1259804717678424070/L-ihu83N_400x400.jpg",
                    name: 'Jatinkkalra',
                    tag: '@Jatinkkalra',
                    socialLinks: {
                        gitlab: '',
                        github: '',
                        telegram: this.$links("telegram_jatinkkalra"),
                        twitter: this.$links("twitter_jatinkkalra")
                    },
                    active: false
                },
                {
                    img: "https://pbs.twimg.com/profile_images/1268951690318958595/u8wVGK5S_400x400.jpg",
                    name: 'TheRuneRanger',
                    tag: '@TheRuneRanger',
                    socialLinks: {
                        gitlab: '',
                        github: '',
                        telegram: '',
                        twitter: this.$links("twitter_theruneranger")
                    },
                    active: false
                },
            ],
            butSirs: {
                firstButSir: {
                    context: this.$t('technicalPage.firstButSir'),
                    isRight: true,
                    height: 227,
                    imgStyle: {
                        maxWidth: '200px',
                        margin: '0 18px 0 -60px'
                    },
                    linkDisabled: [false, false, false, false],
                    imgSrc: require('~/assets/images/thorchain/looking-through-binoculars.svg'),
                    imgAlt: "thorchain node",
                    links: [this.$links('twitter_new_chain'), 
                    this.$links('docs_node_operations'), 
                    this.$links('viewblock'), 
                    this.$links('telegram_dev')]
                },
                secondButSir: {
                    context: this.$t('technicalPage.secondButSir'),
                    isRight: false,
                    height: 296,
                    imgStyle: {
                        maxWidth: '200px',
                        margin: '0 -30px 0 0'
                    },
                    linkDisabled: [false],
                    imgSrc: require('~/assets/images/thorchain/surprised-man.svg'),
                    imgAlt: "thorchain node",
                    externalClasses: ['vault']
                },
                thirdButSir: {
                    context: this.$t('technicalPage.thirdButSir'),
                    isRight: false,
                    height: 429,
                    imgStyle: {
                        maxWidth: '370px'
                    },
                    linkDisabled: [false, false, false],
                    imgSrc: require('~/assets/images/thorchain/thief.svg'),
                    imgAlt: "thorchain node",
                    links: ["dynamic", 
                    this.$links('viewblock_validators'), "/rune"],
                },
                rewiringButSir: {
                    context: this.$t('technicalPage.rewiringButSir'),
                    isRight: false,
                    height: 256,
                    imgStyle: {
                        maxWidth: '400px'
                    },
                    linkDisabled: [true, true, false],
                    imgSrc: require('~/assets/images/thorchain/Sirs.svg'),
                    imgAlt: "thorchain node",
                    links: [null, null, this.$links('docs_midgard_api')]
                },
                wheretoaskButSir: {
                    context: this.$t('technicalPage.wheretoaskButSir'),
                    isRight: true,
                    height: 283,
                    imgStyle: {
                        maxWidth: '254px',
                        marginLeft: '-15px'
                    },
                    linkDisabled: [false, false, false, false],
                    imgSrc: require('~/assets/images/thorchain/helping-wounded-man.svg'),
                    imgAlt: "thorchain node",
                    linkTemp: [0,0,0,1],
                    links: [this.$links('twitter'), 
                    this.$links('telegram_community'), 
                    this.$links('discord'), 
                    this.$links('medium_liquidity_pooling')],
                    linkMargin: 26
                },
                smokeButSir: {
                    context: this.$t('technicalPage.smokeButSir'),
                    isRight: true,
                    height: 256,
                    imgStyle: {
                        maxWidth: '182px',
                        marginLeft: '-30px'
                    },
                    linkDisabled: [false, false, false, false, true],
                    imgSrc: require('~/assets/images/thorchain/friends-talking.svg'),
                    imgAlt: "thorchain node",
                    linkTemp: [0,1,0,0,0],
                    links: [this.$links('bepswap'), 
                    this.$links('medium_liquidity_pooling'), 
                    this.$links('twitter_arbitraging'), 
                    this.$links('docs_node_operations'), ""]
                }
            }
        };
    },
    head() {
        return {

            title: 'Technology - THORChain',
            meta: [
                    {property: "og:locale", content: 'description'},
                    {property: "og:locale", content: "en_US"},
                    {property: "og:type", content: "website"},
                    {property: "og:title", content: "Technology - THORChain"},
                    {property: "og:description", content: "THORChain is a liquidity protocol based on Tendermint & Cosmos-SDK and utilising Threshold Signature Schemes (TSS) to create a marketplace of liquidity for digital assets to be traded in a trustless, permissionless & non-custodial manner."},
                    {property: "og:url", content: process.env.baseUrl + "/technology"},
                    {property: "og:site_name", content: "THORChain"},
                    {property: "og:image", content: process.env.baseUrl + "/technology-meta.png"},
                    {property: "og:image:width", content: "876"},
                    {property: "og:image:heigh", content: "438"},
                    {name: "twitter:creator", content: "@thorchain_org"},
                    {name: "twitter:site", content: "@thorchain_org"},
                    {name: "twitter:title", content: "Technology - THORChain"},
                    {name: "twitter:description", content: "THORChain is a liquidity protocol based on Tendermint & Cosmos-SDK and utilising Threshold Signature Schemes (TSS) to create a marketplace of liquidity for digital assets to be traded in a trustless, permissionless & non-custodial manner."},
                    {name: "twitter:card", content: "summary_large_image"},
                    {name: "twitter:image", content: process.env.baseUrl + "/technology-meta.png"},
            ],
        }

    },
    methods: {
        handleScroll() {
            this.$store.commit({
                type: "scrollChange",
                amount: window.scrollY,
            });
        },
        showThisAvatar(i) {
            this.avatars.forEach((ava, index) => {
                if (index == i) {
                    if (ava.active) {
                        ava.active = false
                        return
                    }
                    ava.active = true
                }
                else
                    ava.active = false
            })
        },
        clickOutside() {
            this.avatars.forEach(ava => {
                ava.active = false;
            })
        }
    },
    mounted() {
        if(this.butSirs && this.butSirs.thirdButSir && this.butSirs.thirdButSir.links)
            this.butSirs.thirdButSir.links[0] = this.asgardActiveVaultLink

        window.addEventListener('mousedown', (e) => {
            // console.log(e.target)
            console.log(e.target.compareDocumentPosition(document.querySelector(".community-pros")))
            if (e.target.compareDocumentPosition(document.querySelector(".community-pros")) !== 10){
                this.clickOutside();
            }
        });
    },
    beforeMount() {

        this.handleScroll();
        window.addEventListener("scroll", this.handleScroll);
        this.butSirs.thirdButSir.links[0] = this.asgardActiveVaultLink
        // console.log("beforeMount");
    },
    beforeDestroy() {
        window.removeEventListener("scroll", this.handleScroll);
        // console.log("beforeDestroy");
    },
    async asyncData({
        store, $services, $links
    }) {
        var minimumBond = await $services.getMinimumBond()
        var runePrice = await $services.getRunePrice()

        if (runePrice && minimumBond) {

            var bondInUsd = parseInt(minimumBond * runePrice);

            minimumBond = utils.numberWithCommas(minimumBond);
            bondInUsd = utils.numberWithCommas(bondInUsd);
        }
        else {
            var bondInUsd = "CONNECTING"
        }

        var asgardActiveVaultLink = await $services.getActiveAsgardVaultLink()
        
        var totalTx = await $services.getThorTotalTransactions()
            if (totalTx)
                totalTx = utils.numberWithCommas(totalTx)
            else
                totalTx = 0

        return {
            minimumBond,
            bondInUsd,
            asgardActiveVaultLink,
            totalTx
        };
    },
};