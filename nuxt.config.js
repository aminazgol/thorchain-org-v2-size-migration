
const isServerlessEnvironment = process.env.ON_VERCEL=="true"
var baseURL = process.env.BASE_URL || 'http://localhost:3000'

export default {
  server: {
    port: 3000,
    host: '0.0.0.0'
  },
  head: {
    title: 'THORChain.org - Decentralized Liquidity Network',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=1500' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>⚡</text></svg>' }
      // <link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>⚡</text></svg>">
    ],
    base: {target:'_blank'}
  },

  css: [
    '@/assets/styles/main.scss',
  ],

  buildModules: ['@nuxtjs/style-resources'],

  styleResources: {
    scss: [
      './assets/styles/_colors.scss',
      ]
  },

  modules: [
    ['nuxt-i18n', {strategy: 'prefix_and_default', lazy: 'true'}],
    '@nuxtjs/axios',
    "@nuxtjs/svg"
  ],

  plugins: [
    {src:'~/plugins/linksV2.js'},
    {src:'~/plugins/services.js'},
    {src:'@/plugins/youtube.js', ssr: false},
  ],

  i18n: {
    locales: [
      {
        code: 'en',
        name: 'English'
      },
      {
        code: 'fr',
        name: 'Français'
      }
    ],
    defaultLocale: 'en',
    vueI18n: {
      fallbackLocale: 'en',
      messages: {
        en: require('./locales/en.json'),
        fr: require('./locales/fr.json')
      }
    }
  },

  components: {
    dirs: [
      '~/components',
        {
          path: '~/components/base/', 
          prefix: 'Base'
        }
    ]
  },
  serverMiddleware: isServerlessEnvironment ? [] : [
    '~/api/rest.js'
  ],
  router: {
    // Run the middleware/user-agent.js on every page
    // middleware: 'redirect'
  },
  env: {
    baseUrl: baseURL,
    midgardBaseUrl: process.env.MIDGARD_BASE_URL || 'https://chaosnet-midgard.bepswap.com',
    FETCH_METHOD: process.env.NODE_ENV === "dev-mock" ? "mock" : "api",
    isProduction: process.env.VERCEL === `1`
  },
  axios:{
    baseURL: 'http://localhost:3000',
    timeout: 3000
  },
  publicRuntimeConfig: {
    axios: {
      browserBaseURL: baseURL
    }
  },
  privateRuntimeConfig: {
    axios: {
      baseURL: baseURL
    },
  },

  loading: false
  // loading: {
  //   color: 'rgba(0,0,0,0)',
  //   height: '0px'
  // }
}