export default ($context, inject, ) => {

    inject('links', (query) => {
        return byString(links, query)
    })
}

function byString(o, s) { // Reference: https://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-and-arrays-by-string-path
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    var a = s.split('.');
    for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in o) {
            o = o[k];
        } else {
            return;
        }
    }
    return o;
}

var links = {
    scrollTo: {
        "technology": ['how-does-it-work', 'how-does-it-compare', 'has-it-been-audited', 'how-active-is-it'],
        "rune": ["what-does-it-do", "how-many-will-exist", "what-influences-it", "where-do-I-get-it", "how-do-I-use-it"]
    },
    navBar: {
        technology: "technology",
        rune: "rune",
        decentralized: "decentralized",
        ecosystem: "ecosystem",
        community: "community",
        roles: "roles",
        economics: "economics",
        docs: "https://docs.thorchain.org/",
        blog: "https://thorchain.medium.com/",
    },
    "nav_docs": {
        "label": "nav_docs",
        "category": "doc",
        "action": "external_link",
        "link": "https://docs.thorchain.org/"
    },
    "nav_blog": {
        "label": "nav_blog",
        "category": "article",
        "action": "external_link",
        "link": "https://thorchain.medium.com/"
    },
    "nav_bepswap": {
      "label": "nav_bepswap",
      "category": "tool",
      "action": "external_link",
      "link": "https://chaosnet.bepswap.com/"
    },
    "nav_shapeshift_mobile_app": {
      "label": "nav_shapeshift_mobile_app",
      "category": "promo",
      "action": "external_link",
      "link": "http://shapeshift.app.link/wDRGTdULMcb"
    },
    "nav_telegram_community": {
      "label": "nav_telegram_community",
      "category": "social",
      "action": "external_link",
      "link": "https://t.me/thorchain_org"
    },
    "nav_gitlab": {
      "label": "nav_gitlab",
      "category": "social",
      "action": "external_link",
      "link": "https://gitlab.com/thorchain"
    },
    "nav_twitter": {
      "label": "nav_twitter",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/thorchain_org"
    },
    "nav_medium_liquidity_pooling": {
      "label": "nav_medium_liquidity_pooling",
      "category": "article",
      "action": "external_link",
      "link": "https://medium.com/thorchain/an-in-depth-guide-to-thorchains-liquidity-pools-c4ea7829e1bf"
    },
    "nav_viewblock": {
      "label": "nav_viewblock",
      "category": "tool",
      "action": "external_link",
      "link": "https://viewblock.io/thorchain"
    },
    "footer_docs_incentive_pendulum": {
      "label": "footer_docs_incentive_pendulum",
      "category": "doc",
      "action": "external_link",
      "link": "https://docs.thorchain.org/how-it-works/incentive-pendulum"
    },
    "footer_docs_fees": {
      "label": "footer_docs_fees",
      "category": "doc",
      "action": "external_link",
      "link": "https://docs.thorchain.org/how-it-works/fees"
    },
    "footer_docs_clp": {
      "label": "footer_docs_clp",
      "category": "doc",
      "action": "external_link",
      "link": "https://docs.thorchain.org/how-it-works/continuous-liquidity-pools"
    },
    "footer_docs_prices": {
      "label": "footer_docs_prices",
      "category": "doc",
      "action": "external_link",
      "link": "https://docs.thorchain.org/how-it-works/prices"
    },
    "footer_docs_governance": {
      "label": "footer_docs_governance",
      "category": "doc",
      "action": "external_link",
      "link": "https://docs.thorchain.org/how-it-works/governance"
    },
    "footer_medium_liquidity_pooling": {
      "label": "footer_medium_liquidity_pooling",
      "category": "article",
      "action": "external_link",
      "link": "https://medium.com/thorchain/an-in-depth-guide-to-thorchains-liquidity-pools-c4ea7829e1bf"
    },
    "footer_telegram_feedback_channel":{
      "label": "footer_telegram_feedback_channel",
      "category": "social",
      "action": "external_link",
      "link": "https://t.me/thorchainfeedback"
    },
    "shapeshift_mobile_app": {
      "label": "shapeshift_mobile_app",
      "category": "promo",
      "action": "external_link",
      "link": "http://shapeshift.app.link/wDRGTdULMcb"
    },
    "asgardex_testnet": {
      "label": "asgardex_testnet",
      "category": "tool",
      "action": "external_link",
      "link": "https://testnet.asgard.exchange"
    },
    "medium_v01": {
      "label": "medium_v01",
      "category": "article",
      "action": "external_link",
      "link": "https://medium.com/@thorchaincommunity/new-thorchain-org-v0-1-62b996980c5"
    },
    "runebridge": {
      "label": "runebridge",
      "category": "tool",
      "action": "external_link",
      "link": "https://runebridge.org/"
    },
    "runebridge_pools": {
      "label": "runebridge_pools",
      "category": "tools",
      "action": "external_link",
      "link": "https://runebridge.org/pools"
    },
    "thorchain_community": {
      "label": "thorchain_community",
      "category": "tool",
      "action": "external_link",
      "link": "https://www.thorchain.community/"
    },
    "coingecko_pair": {
      "label": "coingecko_pair",
      "category": "other",
      "action": "external_link",
      "link": "Varies"
    },
    "coingecko_show_more": {
      "label": "coingecko_show_more",
      "category": "other",
      "action": "external_link",
      "link": "https://www.coingecko.com/en/coins/thorchain#markets"
    },
    "twitter_mehowbrains": {
      "label": "twitter_mehowbrains",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/mehowbrains"
    },
    "twitter_thorchain_org": {
      "label": "twitter_thorchain_org",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/THORChain_org"
    },
    "twitter_show_this_thread": {
      "label": "twitter_show_this_thread",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/mehowbrains/status/1347373080872706049"
    },
    "medium_liquidity_pooling": {
      "label": "medium_liquidity_pooling",
      "category": "article",
      "action": "external_link",
      "link": "https://medium.com/thorchain/an-in-depth-guide-to-thorchains-liquidity-pools-c4ea7829e1bf"
    },
    "twitter_theruneranger": {
      "label": "twitter_theruneranger",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/TheRuneRanger"
    },
    "twitter_show_this_tweet": {
      "label": "twitter_show_this_tweet",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/TheRuneRanger/status/1349460975766921225"
    },
    "twitter_delphi_digital": {
      "label": "twitter_delphi_digital",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/Delphi_Digital"
    },
    "twitter_mperklin": {
      "label": "twitter_mperklin",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/mperklin"
    },
    "twitter_tannedoaksprout": {
      "label": "twitter_tannedoaksprout",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/tannedoaksprout"
    },
    "twitter_cangure91596181": {
      "label": "twitter_cangure91596181",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/CanGure91596181"
    },
    "twitter_jatinkkalra": {
      "label": "twitter_jatinkkalra",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/Jatinkkalra"
    },
    "twitter_bitcoin_sage": {
      "label": "twitter_bitcoin_sage",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/Bitcoin_Sage"
    },
    "twitter__0x_larry": {
      "label": "twitter__0x_larry",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/_0x_larry"
    },
    "twitter_jonny_qi": {
      "label": "twitter_jonny_qi",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/Jonny_Qi"
    },
    "docs_node_operations": {
      "label": "docs_node_operations",
      "category": "doc",
      "action": "external_link",
      "link": "https://docs.thorchain.org/roles/node-operators"
    },
    "twitter_scale_nodes": {
      "label": "twitter_scale_nodes",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/thorchain_org/status/1348126879874617351?s=20"
    },
    "delphi_node_dashboard": {
      "label": "delphi_node_dashboard",
      "category": "tool",
      "action": "external_link",
      "link": "https://defi.delphidigital.io/thorchain/chaosnet/network"
    },
    "viewblock_vaults": {
      "label": "viewblock_vaults",
      "category": "tool",
      "action": "external_link",
      "link": "https://viewblock.io/thorchain/vaults"
    },
    "viewblock_validators": {
      "label": "viewblock_validators",
      "category": "tool",
      "action": "external_link",
      "link": "https://viewblock.io/thorchain/validators"
    },
    "viewblock_pools": {
      "label": "viewblock_pools",
      "category": "tool",
      "action": "external_link",
      "link": "https://viewblock.io/thorchain/pools"
    },
    "thorchain_net_stats": {
      "label": "thorchain_net_stats",
      "category": "tool",
      "action": "external_link",
      "link": "https://thorchain.net/"
    },
    "docs_connecting_thorchain": {
      "label": "docs_connecting_thorchain",
      "category": "doc",
      "action": "external_link",
      "link": "https://docs.thorchain.org/developers/connecting-to-thorchain"
    },
    "twitter_treasury_funding": {
      "label": "twitter_treasury_funding",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/thorchain_org/status/1315850914092052480?s=20"
    },
    "twitter_new_chain": {
      "label": "twitter_new_chain",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/thorchain_org/status/1304211812049534976"
    },
    "viewblock": {
      "label": "viewblock",
      "category": "tool",
      "action": "external_link",
      "link": "https://viewblock.io/thorchain"
    },
    "telegram_dev": {
      "label": "telegram_dev",
      "category": "social",
      "action": "external_link",
      "link": "https://t.me/thorchain_dev"
    },
    "telegram_delphi_digital": {
      "label": "telegram_delphi_digital",
      "category": "social",
      "action": "external_link",
      "link": "https://t.me/DelphiDigitalAlerts"
    },
    "telegram_mperklin": {
      "label": "telegram_mperklin",
      "category": "social",
      "action": "external_link",
      "link": "https://t.me/mperklin"
    },
    "telegram_jatinkkalra": {
      "label": "telegram_jatinkkalra",
      "category": "social",
      "action": "external_link",
      "link": "https://t.me/JatinkkaLra"
    },
    "telegram_bitcoin_sage": {
      "label": "telegram_bitcoin_sage",
      "category": "social",
      "action": "external_link",
      "link": "https://t.me/Bitcoin_Sage"
    },
    "telegram_larrypc": {
      "label": "telegram_larrypc",
      "category": "social",
      "action": "external_link",
      "link": "https://t.me/Larrypc"
    },
    "github_larrypcdotcom": {
      "label": "github_larrypcdotcom",
      "category": "social",
      "action": "external_link",
      "link": "https://github.com/Larrypcdotcom"
    },
    "viewblock_vault_asgard": {
      "label": "viewblock_vault_asgard",
      "category": "tool",
      "action": "external_link",
      "link": "Dynamic"
    },
    "viewblock_vault_yggdrasil": {
      "label": "viewblock_vault_yggdrasil",
      "category": "tool",
      "action": "external_link",
      "link": "https://viewblock.io/thorchain/vaults"
    },
    "docs_midgard_api": {
      "label": "docs_midgard_api",
      "category": "doc",
      "action": "external_link",
      "link": "https://docs.thorchain.org/developers/midgard-api"
    },
    "paper_tss_whitepaper": {
      "label": "paper_tss_whitepaper",
      "category": "paper",
      "action": "external_link",
      "link": "https://github.com/thorchain/Resources/blob/master/Whitepapers/THORChain-TSS-Paper-June2020.pdf"
    },
    "paper_tss_audit": {
      "label": "paper_tss_audit",
      "category": "paper",
      "action": "external_link",
      "link": "https://github.com/thorchain/Resources/blob/master/Audits/THORChain-Kudelski-TSS-Audit-June2020.pdf"
    },
    "paper_tss_benchmark": {
      "label": "paper_tss_benchmark",
      "category": "paper",
      "action": "external_link",
      "link": "https://github.com/thorchain/Resources/blob/master/Whitepapers/THORChain-TSS-Benchmark-July2020.pdf"
    },
    "twitter": {
      "label": "twitter",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/thorchain_org"
    },
    "telegram_community": {
      "label": "telegram_community",
      "category": "social",
      "action": "external_link",
      "link": "https://t.me/thorchain_org"
    },
    "discord": {
      "label": "discord",
      "category": "social",
      "action": "external_link",
      "link": "https://discord.gg/WCDWfgq"
    },
    "bepswap": {
      "label": "bepswap",
      "category": "tool",
      "action": "external_link",
      "link": "https://chaosnet.bepswap.com/"
    },
    "twitter_arbitraging": {
      "label": "twitter_arbitraging",
      "category": "social",
      "action": "external_link",
      "link": "https://twitter.com/mehowbrains/status/1302820452415438848?s=20"
    },
    "sheet_emissions": {
      "label": "sheet_emissions",
      "category": "sheet",
      "action": "external_link",
      "link": "https://docs.google.com/spreadsheets/d/1e5A7TaV6CZtdVqlOSuXSSY7UYiRW9yzd1ST6QTZNqLw/edit#gid=918223980"
    },
    "docs_incentive_pendulum": {
      "label": "docs_incentive_pendulum",
      "category": "doc",
      "action": "external_link",
      "link": "https://docs.thorchain.org/how-it-works/incentive-pendulum"
    },
    "docs_emissions": {
      "label": "docs_emissions",
      "category": "doc",
      "action": "external_link",
      "link": "https://docs.thorchain.org/how-it-works/emission-schedule"
    },
    "docs_transaction_memos": {
      "label": "docs_transaction_memos",
      "category": "doc",
      "action": "external_link",
      "link": "https://docs.thorchain.org/developers/transaction-memos"
    },
    "tools_table": {
      "runestake": {
        "tool_link": {
          "link": "https://www.runestake.info",
          "action": "external_link",
          "category": "tool",
          "label": "runestake"
        },
        "repo_url": null,
        "telegram_url": null,
        "github_url": null,
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/Four4Newt",
          "action": "external_link",
          "category": "social",
          "label": "twitter_four4newt"
        }
      },
      "runedata": {
        "tool_link": {
          "link": "https://www.runedata.info",
          "action": "external_link",
          "category": "tool",
          "label": "runedata"
        },
        "repo_url": {
          "link": "https://github.com/Larrypcdotcom/runedata",
          "action": "external_link",
          "category": "repo",
          "label": "github_runedata"
        },
        "telegram_url": {
          "link": "https://t.me/Larrypc",
          "action": "external_link",
          "category": "social",
          "label": "telegram_larrypc"
        },
        "github_url": {
          "link": "https://github.com/Larrypcdotcom",
          "action": "external_link",
          "category": "social",
          "label": "github_larrypcdotcom"
        },
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/Larrypcdotcom",
          "action": "external_link",
          "category": "social",
          "label": "twitter_larrypcdotcom"
        }
      },
      "runebalance": {
        "tool_link": {
          "link": "https://www.runebalance.com/",
          "action": "external_link",
          "category": "tool",
          "label": "runebalance"
        },
        "repo_url": null,
        "telegram_url": {
          "link": "https://t.me/runebalance",
          "action": "external_link",
          "category": "social",
          "label": "telegram_rebalance"
        },
        "github_url": {
          "link": "https://github.com/SuperZannah",
          "action": "external_link",
          "category": "social",
          "label": "github_superzannah"
        },
        "gitlab_url": {
          "link": "https://gitlab.com/superzannah",
          "action": "external_link",
          "category": "social",
          "label": "gitlab_superzannah"
        },
        "twitter_url": {
          "link": "https://twitter.com/SuperZannah",
          "action": "external_link",
          "category": "social",
          "label": "twitter_superzannah"
        }
      },
      "leaderboard": {
        "tool_link": {
          "link": "https://leaderboard.thornode.org",
          "action": "external_link",
          "category": "tool",
          "label": "leaderboard"
        },
        "repo_url": {
          "link": "https://github.com/tirinox/chaosnetleaders",
          "action": "external_link",
          "category": "repo",
          "label": "github_leaderboard"
        },
        "telegram_url": {
          "link": "https://t.me/account1242",
          "action": "external_link",
          "category": "social",
          "label": "telegram_account1242"
        },
        "github_url": {
          "link": "https://github.com/tirinox",
          "action": "external_link",
          "category": "social",
          "label": "github_tirinox"
        },
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/tirinox",
          "action": "external_link",
          "category": "social",
          "label": "twitter_tirinox"
        }
      },
      "telegram_alerts_bot": {
        "tool_link": {
          "link": "https://t.me/thorchain_alert",
          "action": "external_link",
          "category": "tool",
          "label": "telegram_alerts_bot"
        },
        "repo_url": null,
        "telegram_url": {
          "link": "https://t.me/account1242",
          "action": "external_link",
          "category": "social",
          "label": "telegram_account1242"
        },
        "github_url": {
          "link": "https://github.com/tirinox",
          "action": "external_link",
          "category": "social",
          "label": "github_tirinox"
        },
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/tirinox",
          "action": "external_link",
          "category": "social",
          "label": "twitter_tirinox"
        }
      },
      "telegram_runefaucet_bot": {
        "tool_link": {
          "link": "https://t.me/runefaucetbot",
          "action": "external_link",
          "category": "tool",
          "label": "telegram_runefaucet_bot"
        },
        "repo_url": null,
        "telegram_url": {
          "link": "https://t.me/tgkai",
          "action": "external_link",
          "category": "social",
          "label": "telegram_tgkai"
        },
        "github_url": null,
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/kaiansaari",
          "action": "external_link",
          "category": "social",
          "label": "twitter_kaiansaari"
        }
      },
      "telegram_monitoring_bot": {
        "tool_link": {
          "link": "https://t.me/thorchain_monitoring_bot",
          "action": "external_link",
          "category": "tool",
          "label": "telegram_monitoring_bot"
        },
        "repo_url": {
          "link": "https://github.com/tirinox/thorchainmonitorbot",
          "action": "external_link",
          "category": "repo",
          "label": "github_telegram_monitoring_bot"
        },
        "telegram_url": {
          "link": "https://t.me/account1242",
          "action": "external_link",
          "category": "social",
          "label": "telegram_account1242"
        },
        "github_url": {
          "link": "https://github.com/tirinox",
          "action": "external_link",
          "category": "social",
          "label": "github_tirinox"
        },
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/tirinox",
          "action": "external_link",
          "category": "social",
          "label": "twitter_tirinox"
        }
      },
      "telegram_thor_bot": {
        "tool_link": {
          "link": "https://twitter.com/thor_bot",
          "action": "external_link",
          "category": "tool",
          "label": "telegram_thor_bot"
        },
        "repo_url": {
          "link": "https://github.com/Pusher-Labs/thorchain-twitter-bot",
          "action": "external_link",
          "category": "repo",
          "label": "github_telegram_thor_bot"
        },
        "telegram_url": {
          "link": "https://t.me/stu_k",
          "action": "external_link",
          "category": "social",
          "label": "telegram_stu_k"
        },
        "github_url": {
          "link": "https://github.com/stuartkuentzel",
          "action": "external_link",
          "category": "social",
          "label": "github_stuartkuentzel"
        },
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/swkuentzel",
          "action": "external_link",
          "category": "social",
          "label": "twitter_swkuentzel"
        }
      },
      "twitter_thorchainhammer": {
        "tool_link": {
          "link": "https://twitter.com/thorchainhammer",
          "action": "external_link",
          "category": "tool",
          "label": "twitter_thorchainhammer"
        },
        "repo_url": null,
        "telegram_url": {
          "link": "https://t.me/mehowbrains",
          "action": "external_link",
          "category": "social",
          "label": "telegram_mehowbrains"
        },
        "github_url": null,
        "gitlab_url": {
          "link": "https://gitlab.com/mehowbrains",
          "action": "external_link",
          "category": "social",
          "label": "gitlab_mehowbrains"
        },
        "twitter_url": {
          "link": "https://twitter.com/mehowbrains",
          "action": "external_link",
          "category": "social",
          "label": "twitter_mehowbrains"
        }
      },
      "viewblock": {
        "tool_link": {
          "link": "https://viewblock.io/thorchain",
          "action": "external_link",
          "category": "tool",
          "label": "viewblock"
        },
        "repo_url": null,
        "telegram_url": null,
        "github_url": {
          "link": "https://github.com/Ashlar",
          "action": "",
          "category": "",
          "label": "github_ashlar"
        },
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/view_block",
          "action": "external_link",
          "category": "social",
          "label": "twitter_view_block"
        }
      },
      "thorchain_net": {
        "tool_link": {
          "link": "https://thorchain.net/",
          "action": "external_link",
          "category": "tool",
          "label": "thorchain_net"
        },
        "repo_url": {
          "link": "https://github.com/Pusher-Labs/thorchain-explorer",
          "action": "external_link",
          "category": "repo",
          "label": "github_thorchain_net"
        },
        "telegram_url": {
          "link": "https://t.me/stu_k",
          "action": "external_link",
          "category": "social",
          "label": "telegram_stu_k"
        },
        "github_url": {
          "link": "https://github.com/stuartkuentzel",
          "action": "external_link",
          "category": "social",
          "label": "github_stuartkuentzel"
        },
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/swkuentzel",
          "action": "external_link",
          "category": "social",
          "label": "twitter_swkuentzel"
        }
      },
      "thorchain_community": {
        "tool_link": {
          "link": "https://thorchain.community",
          "action": "external_link",
          "category": "tool",
          "label": "thorchain_community"
        },
        "repo_url": null,
        "telegram_url": {
          "link": "https://t.me/mehowbrains",
          "action": "external_link",
          "category": "social",
          "label": "telegram_mehowbrains"
        },
        "github_url": null,
        "gitlab_url": {
          "link": "https://gitlab.com/mehowbrains",
          "action": "external_link",
          "category": "social",
          "label": "gitlab_mehowbrains"
        },
        "twitter_url": {
          "link": "https://twitter.com/mehowbrains",
          "action": "external_link",
          "category": "social",
          "label": "twitter_mehowbrains"
        }
      },
      "delphi_dashboard": {
        "tool_link": {
          "link": "https://defi.delphidigital.io/thorchain",
          "action": "external_link",
          "category": "tool",
          "label": "delphi_dashboard"
        },
        "repo_url": null,
        "telegram_url": {
          "link": "https://t.me/captainlk",
          "action": "external_link",
          "category": "social",
          "label": "telegram_captainlk"
        },
        "github_url": null,
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/lukedelphi",
          "action": "external_link",
          "category": "social",
          "label": "twitter_lukedelphi"
        }
      },
      "twitter_rune_coins": {
        "tool_link": {
          "link": "https://twitter.com/mehowbrains/status/1336593711766114304",
          "action": "external_link",
          "category": "tool",
          "label": "twitter_rune_coins"
        },
        "repo_url": null,
        "telegram_url": {
          "link": "https://t.me/mehowbrains",
          "action": "external_link",
          "category": "social",
          "label": "telegram_mehowbrains"
        },
        "github_url": null,
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/mehowbrains",
          "action": "external_link",
          "category": "social",
          "label": "twitter_mehowbrains"
        }
      },
      "asgardex_testnet": {
        "tool_link": {
          "link": "https://testnet.asgard.exchange/",
          "action": "external_link",
          "category": "tool",
          "label": "asgardex_testnet"
        },
        "repo_url": {
          "link": "https://github.com/Pusher-Labs/asgard-exchange",
          "action": "external_link",
          "category": "repo",
          "label": "github_asgardex"
        },
        "telegram_url": {
          "link": "https://t.me/stu_k",
          "action": "external_link",
          "category": "social",
          "label": "telegram_stu_k"
        },
        "github_url": {
          "link": "https://github.com/stuartkuentzel",
          "action": "external_link",
          "category": "social",
          "label": "github_stuartkuentzel"
        },
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/swkuentzel",
          "action": "external_link",
          "category": "social",
          "label": "twitter_swkuentzel"
        }
      },
      "xchainjs": {
        "tool_link": {
          "link": "https://xchainjs.org",
          "action": "external_link",
          "category": "tool",
          "label": "xchainjs"
        },
        "repo_url": {
          "link": "https://github.com/xchainjs/xchainjs-lib",
          "action": "external_link",
          "category": "repo",
          "label": "github_xchainjs"
        },
        "telegram_url": {
          "link": "https://t.me/xchainjs",
          "action": "external_link",
          "category": "social",
          "label": "telegram_xchainjs"
        },
        "github_url": {
          "link": "https://github.com/xchainjs/xchainjs-lib",
          "action": "external_link",
          "category": "social",
          "label": "github_xchain"
        },
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/xchainjs",
          "action": "external_link",
          "category": "social",
          "label": "twitter_xchainjs"
        }
      },
      "xdefi": {
        "tool_link": {
          "link": "https://www.xdefi.io",
          "action": "external_link",
          "category": "tool",
          "label": "xdefi"
        },
        "repo_url": null,
        "telegram_url": {
          "link": "https://t.me/xdefi_io",
          "action": "external_link",
          "category": "social",
          "label": "telegram_xdefi_io"
        },
        "github_url": null,
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/xdefi_wallet",
          "action": "external_link",
          "category": "social",
          "label": "twitter_xdefi_wallet"
        }
      },
      "telegram_thornode_bot": {
        "tool_link": {
          "link": "https://github.com/block42-blockchain-company/thornode-telegram-bot",
          "action": "external_link",
          "category": "tool",
          "label": "telegram_thornode_bot"
        },
        "repo_url": {
          "link": "https://github.com/block42-blockchain-company/thornode-telegram-bot",
          "action": "external_link",
          "category": "repo",
          "label": "github_telegram_thornode_bot"
        },
        "telegram_url": {
          "link": "https://t.me/block42_crypto",
          "action": "external_link",
          "category": "social",
          "label": "telegram_block42_crypto"
        },
        "github_url": {
          "link": "https://github.com/block42-blockchain-company",
          "action": "external_link",
          "category": "social",
          "label": "github_block42_crypto"
        },
        "gitlab_url": null,
        "twitter_url": {
          "link": "https://twitter.com/block42_company",
          "action": "external_link",
          "category": "social",
          "label": "twitter_block42_company"
        }
      }
    }
  }