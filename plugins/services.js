export default ({ $axios, store, $links}, inject) =>{
    // console.log("$fetchData called")
    inject('services', {
        async getLastBlock(){
            try{
                const url = process.env.baseUrl + "/api/static_data/lastBlock";
                var res = await $axios.get(url)
                var data = res.data
                return data
            } catch(e){
                return null
            }
        },
        async getMinimumBond(){
            try{
                var url = process.env.baseUrl + '/api/static_data/minBond';
                var res = await $axios.get(url)
                var minimumBond = res.data
                minimumBond = minimumBond / 100000000; // in Rune
                minimumBond = parseInt(minimumBond)
                return minimumBond
            }catch(e){
                return null
            }
        },
        async getRunePrice(){
            try{
                var url = process.env.baseUrl + '/api/static_data/runePrice';
                var res = await $axios.get(url)
                return res.data
            } catch(e){
                return null
            }
        },
        async getActiveAsgardVaultLink(){
            try{

                var url = process.env.baseUrl + '/api/static_data/activeAsgardVaultLink'
                var res = await $axios.get(url)
                var link =  res.data
                console.log(typeof $links)
                var linkProps = $links('viewblock_vault_asgard')
                linkProps.link = link
                return linkProps
            } catch(e){
                console.log(e)
                return null
            }
        },
        async getThorTotalTransactions(){
            try{
                var url = process.env.baseUrl + '/api/static_data/thorTotalTx'
                var res = await $axios.get(ur)
                return res.data
            }
            catch(e){
                return null
            }
        },
        async getHistoricalVolume(from, to){
            console.log("get historical volume")
            var data = await fetch(
                `https://chaosnet-midgard.bepswap.com/v1/history/total_volume?interval=day&from=${from}&to=${to}`
              ).then((res) => res.json());
              var usdData = await fetch(
                `https://chaosnet-midgard.bepswap.com/v1/history/pools?pool=BNB.BUSD-BD1&interval=day&from=${from}&to=${to}`
              ).then((res) => res.json());
            console.log(data)
            return {data, usdData}
        },
        async getCoingeckoData(){
            try{
                const url = process.env.baseUrl + '/api/coingecko'
                var res = await $axios.get(url)
                if(res.data){
                    var data = res.data
                    data['valid'] = true
                }
                else
                var data = {data:null, updatedAt:null, valid: false}
                return data
            }catch(e){
                return null
            }
        },
        async getThorNetValues(){
            try {
                var res = await $axios.get(process.env.baseUrl + '/api/static_data/thorNetValues')
                var netValues = res.data
                return netValues
              } catch (e) {
                console.log("fetching ThorNetValues failed")
                console.log(e)
                return null
              }
        },
        async getPoolsTableData(){
            var res = this.$axios.$get('/api/static_data/poolsTableData')
            return res
        }
    })
}