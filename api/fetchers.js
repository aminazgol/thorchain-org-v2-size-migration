const utils = require('../utils');
const https = require('https');
const axios = require('axios')
const midgardBaseUrl = 'https://chaosnet-midgard.bepswap.com'
const {reportError} = require('./errorHandling.js');
const { parse } = require('path');

async function downloadCoingeckoMarkets() {
    return new Promise((resolve, reject) => {

        const req = https.get('https://api.coingecko.com/api/v3/coins/thorchain/tickers?depth=true', (res) => {
            var dataStr = ""
            res.on('data', d => {
                dataStr += d;
            })
            res.on('end', () => {
                resolve(JSON.parse(dataStr))
            })
        })
        req.on('error', error => {
            reject(error)
        })
    })
}
function parseCoingeckoMarkets(data) {
    var parsed = []
    for (var ticker of data.tickers) {
        try{
            parsed.push({
                market: ticker.market.name,
                base: ticker.base,
                target: ticker.target,
                pair: ticker.target + "/" + ticker.base,
                depth_up: ticker.cost_to_move_up_usd,
                depth_down: ticker.cost_to_move_down_usd,
                price: ticker.converted_last.usd,
                spread: ticker.bid_ask_spread_percentage.toFixed(2),
                volume: ticker.converted_volume.usd,
                tradeUrl: ticker.trade_url
            })
        }
        catch(e){}
    }
    // parsed.sort((a,b)=> b.volume - a.volume)
    return parsed
}

async function downloadCoingeckoNetInfo() {
    return new Promise((resolve, reject) => {
        const url = 'https://api.coingecko.com/api/v3/coins/thorchain?community_data=false&developer_data=false&tickers=false&localization'
        const req = https.get(url, (res) => {
            var dataStr = ""
            res.on('data', d => {
                dataStr += d;
            })
            res.on('end', () => {
                resolve(JSON.parse(dataStr))
            })
        })
        req.on('error', error => {
            reject(error)
        })
    })

}

class Fetchers {

    async fetchCoingeckoMarkets() {
        try{
            var fetched = await axios.get('https://api.coingecko.com/api/v3/coins/thorchain/tickers?depth=true')
            var parsed = parseCoingeckoMarkets(fetched.data)
            return parsed.slice(0, 9)
        } catch(e){
            reportError("fetching coingecko markets failed", e)
            return null
        }
    }


    async fetchCoingeckoNetInfo() {
        try{
            var res = await axios.get('https://api.coingecko.com/api/v3/coins/thorchain?community_data=false&developer_data=false&tickers=false&localization')
            var retval = {circulatingSupply: res.data.market_data.circulating_supply}
            return retval
        } catch(e){
            reportError("fetching Thorchain network info from coingecko failed", e)
            return null
        }
    }
    

    async fetchLastBlock() {
        const url = midgardBaseUrl + "/v1/thorchain/lastblock"
        try{
            var res = await axios.get(url)
            var data = res.data.thorchain
            return data
        } catch(e){
            reportError("req failed: "+url, e)
            return null
        }

    }
    async fetchMinimumBond() {
        var url = midgardBaseUrl + "/v1/network";
        try{
            var res = await axios.get(url)
            var minimumBond = res.data.bondMetrics.minimumActiveBond
            minimumBond = parseInt(minimumBond)
            return minimumBond
        } catch(e){
            reportError("req failed "+ url,  e)
            return null
        }
    }
    async fetchRunePrice() {
        var url = midgardBaseUrl + "/v1/assets?asset=BNB.BUSD-BD1";
        try{
            var res = await axios.get(url)
            var usdInRune = res.data[0].priceRune
            usdInRune = parseFloat(usdInRune)
            var runeInUsd = 1 / usdInRune
            return runeInUsd
        } catch(e){
            reportError("req failed "+ url,  e)
            return null
        }   
    }

    async fetchActiveAsgardVaultLink() {
        var url = "https://chaosnet-seed.thorchain.info/"
        
        try{
            var res = await axios.get(url)
            var nodeIps = res.data
            var nodeIp = nodeIps[0]
            var nodePort = 1317
            url = `http://${nodeIp}:${nodePort}/thorchain/pool_addresses`
            res = await axios.get(url)
            var poolAddress = res.data.current[0].address
            var link = "https://viewblock.io/thorchain/address/" + poolAddress
            return link
        } catch(e){
            reportError("req failed "+ url,  e)
            return null
        }
    }
        
    async fetchThorNetValues() {
        var url = midgardBaseUrl + "/v1/network";
        try{
            var res = await axios.get(url)
            var retval = {
                totalStaked: res.data.totalStaked,
                totalActiveBond: res.data.bondMetrics.totalActiveBond,
                totalStandbyBond: res.data.bondMetrics.totalStandbyBond,
                totalReserve: res.data.totalReserve,
                bondingAPY: res.data.bondingAPY,
                liquidityAPY: res.data.liquidityAPY,
                activeNodeCount: res.data.activeNodeCount,
                standbyNodeCount: res.data.standbyNodeCount,
            }
            retval['totalCapital'] = parseInt(retval.totalActiveBond)
            + parseInt(retval.totalReserve)
            + parseInt(retval.totalStandbyBond)
            + parseInt(retval.totalStaked)
            
            
            url = midgardBaseUrl + '/v1/stats'
            res = await axios.get(url)
            retval['totalEarned'] = res.data.totalEarned
            retval['totalTx'] = res.data.totalTx
            retval['totalTx24h'] = res.data.dailyTx
            retval['volume24h'] = res.data.totalVolume24hr
            retval['totalUsers'] = res.data.totalUsers

            return retval
    
        } catch(e){
            reportError("req failed "+ url,  e)
            return null
        }
    }

    async fetchTotalTXs() {
        var url = 'https://api.viewblock.io/thorchain/txs?page=1&network=testnet&type=all'
        try{
            var res = await axios.get(url)
            return res.data.total

        } catch(e){
            reportError("req failed "+ url,  e)
            return null
        }
    }

    async fetchViewBlockTotalTx() {
        var url = 'https://api.viewblock.io/thorchain/txs?page=1&network=testnet'
        var headers = {
            "Host": "api.viewblock.io",
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0",
            "Accept": "application/json",
            "Accept-Language": "en-US,en;q=0.5",
            "Accept-Encoding": "gzip, deflate, br",
            "Referer": "https://viewblock.io/thorchain/txs",
            "Content-Type": "application/json",
            "Origin": "https://viewblock.io",
            "DNT": 1,
            "Connection": "keep-alive",
            "Pragma": "no-cache",
            "Cache-Control": "no-cache"
        }
        try{
            var res = await axios.get(url, { headers })
            if (res.data)
                return res.data.total
            else
                return null
        } catch(e){
            reportError("req failed "+ url, e)
            return null
        }
    }
    async fetchPoolsTableData(){
        /* fetch Pool names*/
        var url = midgardBaseUrl + '/v1/pools?status=enabled'
        try{
            var res = await axios.get(url)
            var pools = res.data
            
            /* get Pools details */
            url = midgardBaseUrl + '/v1/pools/detail?view=simple&asset='
            for(var i in pools){
                url += pools[i]
                if(i != pools.length - 1)
                url += ','
            }
            res = await axios.get(url)
            var poolsDetails = res.data
            
            /*Sort Pools based on `Dept`*/
            poolsDetails.sort((a, b)=>{
                if(!a.hasOwnProperty('runeDepth') || !b.hasOwnProperty('runeDepth'))
                return -1 * Number.MAX_SAFE_INTEGER
                var diff = parseInt(b.runeDepth) - parseInt(a.runeDepth)
                return diff
            })
            
            /* pick the top ones */
            var topPools = poolsDetails.slice(0, 10)
            
            /* get Rune Price to calculate depth in rune */
            var runePrice = await (new Fetchers()).fetchRunePrice()
            
            /* parse values */
            var finalPools = []
            for(var pool of topPools){
                finalPools.push(
                    {
                        chain: pool.asset.split('.')[0],
                        symbol: pool.asset.split('.')[1],
                        depth: pool.runeDepth / 100000000 * runePrice,
                        apy: pool.poolAPY,
                        volume24h: pool.poolVolume24hr / 100000000 * runePrice
                    }
                )
            }
            return finalPools
        } catch(e){
            reportError("req failed "+ url,  e)
            return null
        }
    }
}
        
module.exports = new Fetchers()