const fetchers = require('./fetchers')

async function test(){
    var res = await fetchers.fetchViewBlockTotalTx()
    console.log(res)
}
async function testPoolsTableData(){
    var func = fetchers.fetchLastBlock

    var res = await func();

}
// testPoolsTableData()
function testAsyncUpdate(){
    async function updatea(){
        return "a --- "
    }
    function updateb(){
        return new Promise(resolve =>{
            setTimeout(() => {
                resolve("b ---")
            }, 3);
        })
    }
    async function updatec(){
        return "c ---"
    }
    var data = {
        'a': {f: updatea, value: null},
        'b': {f: updateb, value: null},
        'c': {f: updatec, value: null},
    }
    setTimeout(async () => {
        for (var i of Object.keys(data)){
            (() => {
                var record = data[i]
                record.f().then((res)=>{
                    record.value = res
                    console.log(data)
                })
            })()
        }
    }, 0);
}

testAsyncUpdate()